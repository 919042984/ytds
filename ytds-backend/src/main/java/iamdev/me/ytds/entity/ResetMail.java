package iamdev.me.ytds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@TableName("tb_reset_mail")
public class ResetMail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 重置id
     */
    @TableId(value = "reset_id", type = IdType.AUTO)
    private Integer resetId;
    /**
     * 重置邮箱
     */
    private String email;
    /**
     * 重置密码代码
     */
    private String code;
    /**
     * 发送时间
     */
    @TableField("send_date")
    private Date sendDate;


    public Integer getResetId() {
        return resetId;
    }

    public void setResetId(Integer resetId) {
        this.resetId = resetId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    @Override
    public String toString() {
        return "ResetMail{" +
            ", resetId=" + resetId +
            ", email=" + email +
            ", code=" + code +
            ", sendDate=" + sendDate +
            "}";
    }
}
