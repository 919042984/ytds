package iamdev.me.ytds.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@ConfigurationProperties(prefix = "es")
@EnableConfigurationProperties(EsConfig.class)
@EnableElasticsearchRepositories(basePackages = "iamdev.me.ytds.repository")
@Data
public class EsConfig {

    private Integer port;
    private String ip;


}
