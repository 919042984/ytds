package iamdev.me.ytds.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import iamdev.me.ytds.entity.Doc;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
public interface DocMapper extends BaseMapper<Doc> {

    List<Doc> findDoc(Page page, Map<String, Object> params);

    Map queryDocTypeNumber(@Param("docUserId") Integer docUserId);
}
