package iamdev.me.ytds.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import iamdev.me.ytds.entity.LoginLog;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
public interface LoginLogMapper extends BaseMapper<LoginLog> {

}
