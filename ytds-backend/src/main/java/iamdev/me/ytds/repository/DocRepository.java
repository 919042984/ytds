package iamdev.me.ytds.repository;

import iamdev.me.ytds.entity.Doc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface DocRepository extends ElasticsearchRepository<Doc, String> {
}
