package iamdev.me.ytds;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan("iamdev.me.ytds.mapper")
@Configuration
public class YTDSApplication {




	public static void main(String[] args) {
		SpringApplication.run(YTDSApplication.class, args);
	}
}
